#!/bin/sh

echo "Test for non existent default log:"
./check_borg

echo
echo "Test for non existent log:"
./check_borg -l not-there

echo
echo "Test for exit states 0:"
touch fixtures/create_0_prune_0
./check_borg -l fixtures/create_0_prune_0

echo
echo "Test for create exit state 1:"
./check_borg -l fixtures/create_1_prune_0

echo
echo "Test for create exit state 2:"
./check_borg -l fixtures/create_2_prune_0

echo
echo "Test for prune exit state 1:"
./check_borg -l fixtures/create_0_prune_1

echo
echo "Test for prune exit state 2:"
./check_borg -l fixtures/create_0_prune_2

echo
echo "Test for unreadable log file:"
./check_borg -l fixtures/unreadable

echo
echo "Test for unreadable prune in log file:"
./check_borg -l fixtures/unreadable_prune
